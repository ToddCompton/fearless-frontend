import React from 'react';
import Nav from './Nav';
import './index.css';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <React.Fragment>
    <Nav />

    <div className="container">
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Conference</th>
          </tr>
        </thead>
        <tbody>
        {props.attendees.map(attendee => {
          return (
            <tr key={attendee.href}>
              <td className="w-50">{ attendee.name }</td>
              <td className="w-50">{ attendee.conference }</td>
            </tr>          
          );
        })}
        </tbody>
      </table>
    </div>
    </React.Fragment>
  );
}

export default App;
