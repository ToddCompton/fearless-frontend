
window.addEventListener('DOMContentLoaded', async () => {

    function createCard(name, description, pictureUrl, startDate, endDate) {
        return `
          <div class="shadow p-3 mb-5 mt-1 pl-10 pr-10 sm-white rounded">
            <div class="card">
              <img src="${pictureUrl}" class="card-img-top">
              <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text">${description}</p>
              </div>            
              <div class="card-footer mt-auto text-center text-muted">
                ${startDate} - ${endDate}
              </div>
            </div>
          </div>
        `;
      }
      

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {

      // Figure out what to do when the response is bad
      // Figure out what to do if an error is raised
      // Go here to figure it out https://getbootstrap.com/docs/5.1/components/alerts/
      return `
      <div class="alert alert-warning" role="alert">
        A simple warning alert—check it out!
      </div>
      `
      } else {
        const data = await response.json();
        let colCount = 1;
  
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const starts = details.conference.starts;
              const ends = details.conference.ends;
              const startDate = new Date(starts).toLocaleDateString();
              console.log("startDate", startDate);
              const endDate = new Date(ends).toLocaleDateString();
              console.log("endDate", endDate);
              const html = createCard(title, description, pictureUrl, startDate, endDate);
              const column = document.querySelector('.col'+ colCount);
              column.innerHTML += html;
              console.log(html);
              colCount +=1;
              if (colCount > 3) {
                colCount = 1
              }
            }
          }
    
        }
    } catch (e) {
        console.error(e)
      // Figure out what to do if an error is raised
      // Go here to figure it out https://getbootstrap.com/docs/5.1/components/alerts/
      return `
        <div class="alert alert-warning" role="alert">
          A simple warning alert—check it out!
        </div>
        `
    }
  
  });
  
  
